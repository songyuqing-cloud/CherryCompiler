/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Base.hpp"

namespace text {

    template<typename T>
    [[nodiscard]] inline constexpr bool
    isDigit(T codePoint) noexcept {
        return codePoint >= '0' && codePoint <= '9';
    }

    template<typename T>
    [[nodiscard]] inline constexpr bool
    isASCIIAlphaUpper(T codePoint) noexcept {
        return codePoint >= 'A' && codePoint <= 'Z';
    }

    template<typename T>
    [[nodiscard]] inline constexpr bool
    isASCIIAlphaLower(T codePoint) noexcept {
        return codePoint >= 'a' && codePoint <= 'z';
    }

    template<typename T>
    [[nodiscard]] inline constexpr bool
    isASCIIAlpha(T codePoint) noexcept {
        return isASCIIAlphaUpper(codePoint) || isASCIIAlphaLower(codePoint);
    }

} // namespace text
