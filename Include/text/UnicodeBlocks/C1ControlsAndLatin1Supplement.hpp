/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * https://www.unicode.org/charts/PDF/U0080.pdf
 */

#pragma once

#include "../Base.hpp"

namespace text {

    constexpr CodePoint NO_BREAK_SPACE = 0x00A0; // NBSP

} // namespace text
