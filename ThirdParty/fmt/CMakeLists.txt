# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

configure_file(fmt.cmake CMakeLists.txt)

execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

message(WANING "dir: ${CMAKE_CURRENT_BINARY_DIR}")
if (result)
    message(FATAL_ERROR "CMake step for fmt failed: ${result}")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --build .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

if (result)
    message(FATAL_ERROR "Build step for fmt failed: ${result}")
endif()

add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/fmt-src
        ${CMAKE_CURRENT_BINARY_DIR}/fmt-build
        EXCLUDE_FROM_ALL)
