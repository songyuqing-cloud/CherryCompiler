/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace base::ExitCode {

    constexpr const int Success = 0;
    constexpr const int UnspecifiedError = 1;
    constexpr const int NoFileSpecified = 2;
    constexpr const int FileNotFound = 3;

    constexpr const int LexerFailure = 0x100;

    constexpr const int ParserFailure = 0x200;

    constexpr const int CodeGeneratorFailure = 0x300;

} // namespace base::ExitCode
