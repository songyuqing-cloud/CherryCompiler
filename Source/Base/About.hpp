/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/Base/Version.hpp"

namespace base::About {

    constexpr const std::string_view name{"Cherry"};
    constexpr const Version version{CHERRY_VERSION_MAJOR, CHERRY_VERSION_MINOR, CHERRY_VERSION_PATCH};

} // namespace base::About
