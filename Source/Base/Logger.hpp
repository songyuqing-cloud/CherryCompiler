/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>
#include <utility> // for std::forward

#include <cstdio> // for stdout

#include <fmt/format.h> // for fmt::format

namespace logger {

    [[nodiscard]] std::string_view
    currentDateTime() noexcept;

    template <typename...Args>
    inline void
    log(const char *tag, std::string_view format, Args &&...args) noexcept {
        fmt::print(stdout,
                   fmt::format("[{}] [{}] {}\n", currentDateTime(), tag, format),
                   std::forward<Args>(args)...);
    }

#define BASE_LOGGER_DEFINE(symbolName, formatName) \
    template <typename...Args> \
    inline void \
    symbolName(std::string_view format, Args &&...args) noexcept { \
        log(formatName, format, std::forward<Args>(args)...); \
    }

    BASE_LOGGER_DEFINE(debug, "DEBUG")
    BASE_LOGGER_DEFINE(error, "ERROR")
    BASE_LOGGER_DEFINE(info, "INFO")
    BASE_LOGGER_DEFINE(trace, "TRACE")
    BASE_LOGGER_DEFINE(warning, "WARNING")

} // namespace logger
