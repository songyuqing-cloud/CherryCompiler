/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Base/FileInput.hpp"

#include "Source/Base/Assert.hpp"

namespace base {

    FileInput::FileInput(const std::string &fileName) noexcept {
        m_file = std::fopen(fileName.c_str(), "rb");
        if (m_file == nullptr)
            return;

        std::fseek(m_file, 0L, SEEK_END);
        const auto size = std::ftell(m_file);
        std::rewind(m_file);

        if (size == -1) {
            std::fclose(m_file);
            m_file = nullptr;
            return;
        }

        m_size = static_cast<std::size_t>(size);
    }

    FileInput::~FileInput() noexcept {
        if (m_file)
            std::fclose(m_file);
    }

    bool
    FileInput::isEOF() const noexcept {
        return std::feof(m_file) == 1;
    }

    bool
    FileInput::readChar(char32_t *out) noexcept {
        if (m_file == nullptr)
            return false;

        char8_t character{};
        if (std::fread(&character, sizeof(character), 1, m_file) != 1)
            return false;

        // TODO https://encoding.spec.whatwg.org/#utf-8-decoder
        CHERRY_ASSERT(character <= 0x7F);
        *out = static_cast<char32_t>(character);
        return true;
    }

} // namespace base
