/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/CodeGenerator/FunctionCodeBlock.hpp"

class Application {
    const std::string_view m_fileName;

public:
    [[nodiscard]] inline constexpr explicit
    Application(std::string_view fileName) noexcept
            : m_fileName(fileName) {
    }

    [[nodiscard]] static int
    executeFunction(const codegen::FunctionCodeBlock &) noexcept;

    [[nodiscard]] int
    run() noexcept;
};
