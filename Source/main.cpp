/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include <filesystem>
#include <string_view>

#include <cstddef>

#include "Source/Application.hpp"
#include "Source/Base/About.hpp"
#include "Source/Base/ExitCode.hpp"
#include "Source/Base/Logger.hpp"

[[nodiscard]] int
printInformationAndExit() noexcept;

int main(int argc, char *argv[]) noexcept {
    std::string_view fileName{};
    for (std::size_t i = 1; i < static_cast<std::size_t>(argc); ++i) {
        const std::string_view arg{argv[i]};
        if (arg.starts_with("--")) {
            if (arg == "--about"
                    || arg == "--copying"
                    || arg == "--copyright"
                    || arg == "--info"
                    || arg == "--information"
                    || arg == "--version") {
                return printInformationAndExit();
            }
        } else if (std::empty(fileName)) {
            fileName = arg;
        } else {
            logger::error("Multiple source files aren't supported!");
        }
    }

    if (std::empty(fileName)) {
        logger::error("No file specified!");
        return base::ExitCode::NoFileSpecified;
    }

    if (!std::filesystem::exists(fileName)) {
        logger::error("File not found: {}", fileName);
        return base::ExitCode::FileNotFound;
    }

    Application application{fileName};
    return application.run();
}

int
printInformationAndExit() noexcept {
    std::printf("Cherry Compiler\n"
                "Version: %hhu.%hhu.%hhu\n"
                "\n"
                "Copyright (C) 2021 Tristan Gerritsen\n"
                "All Rights Reserved\n"
                "Licensed under the Mozilla Public License 2.0\n",
                base::About::version.major(), base::About::version.minor(), base::About::version.patch());
    return base::ExitCode::Success;
}
