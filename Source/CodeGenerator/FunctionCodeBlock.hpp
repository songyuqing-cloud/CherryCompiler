/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility> // for std::move
#include <vector>

#include "Source/Base/CompilerFeatures.hpp"

namespace codegen {

    struct FunctionCodeBlock {
        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        FunctionCodeBlock(std::string &&functionName, std::vector<char8_t> &&code={}) noexcept
                : m_functionName(std::move(functionName))
                , m_code(std::move(code)) {
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR std::vector<char8_t> &
        code() noexcept {
            return m_code;
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR  const std::vector<char8_t> &
        code() const noexcept {
            return m_code;
        }

        [[nodiscard]] inline std::string &
        functionName() noexcept {
            return m_functionName;
        }

        [[nodiscard]] inline const std::string &
        functionName() const noexcept {
            return m_functionName;
        }

    private:
        std::string m_functionName;
        std::vector<char8_t> m_code;
    };

} // namespace codegen

