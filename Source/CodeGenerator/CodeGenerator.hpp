/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/CodeGenerator/FunctionCodeBlock.hpp"
#include "Source/Parser/Forward.hpp"

namespace codegen {

    class CodeGenerator {
        const parser::RootNode *m_rootNode{};
        std::vector<FunctionCodeBlock> m_functionCodeBlocks{};

    public:
        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR explicit
        CodeGenerator(const parser::RootNode *rootNode) noexcept
                : m_rootNode(rootNode) {
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR std::vector<FunctionCodeBlock> &
        functionCodeBlocks() noexcept {
            return m_functionCodeBlocks;
        }

        [[nodiscard]] inline CHERRY_CONSTEXPR_VECTOR const std::vector<FunctionCodeBlock> &
        functionCodeBlocks() const noexcept {
            return m_functionCodeBlocks;
        }

        [[nodiscard]] bool
        processFunctionDefinition(const parser::FunctionDefinitionNode *) noexcept;

        [[nodiscard]] bool
        run() noexcept;
    };

} // namespace codegen
