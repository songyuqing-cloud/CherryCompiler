/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include <limits>

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/CodeGenerator/CodeGenerator.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"
#include "Source/Parser/FunctionDefinitionNode.hpp"
#include "Source/Parser/Node.hpp"
#include "Source/Parser/ReturnStatementNode.hpp"

namespace codegen {

    [[nodiscard]] bool
    setReturnValue(FunctionCodeBlock &block, std::uint64_t value) noexcept {
        if (value == 0) {
            block.code().push_back(0x31); // xor eax,
            block.code().push_back(0xc0); // eax
            return true;
        }

        if (value <= std::numeric_limits<std::uint32_t>::max()) {
            block.code().push_back(0xb8); // mov eax,

            const auto *arr = reinterpret_cast<const char8_t *>(&value);
            block.code().push_back(arr[0]);
            block.code().push_back(arr[1]);
            block.code().push_back(arr[2]);
            block.code().push_back(arr[3]);
            return true;
        }

        // TODO
        return false;
    }

    [[nodiscard]] bool
    processReturnStatement(FunctionCodeBlock &block, const parser::ReturnStatementNode *node) noexcept {
        if (node->expression() != nullptr) {
            if (node->expression()->type() != parser::ExpressionType::PRIMARY_UNSIGNED) {
                logger::error("CodeGenerator: invalid return expression: {}",
                              parser::toString(node->expression()->type()));
                return false;
            }

            const auto value = static_cast<const parser::PrimaryExpression<std::uint64_t> *>(node->expression())->data();
            if (!setReturnValue(block, value)) {
                logger::trace("CodeGenerator: failed to set return value: {}", value);
                return false;
            }
        }

        block.code().push_back(0xc3); // ret
        return true;
    }

    bool
    CodeGenerator::processFunctionDefinition(const parser::FunctionDefinitionNode *node) noexcept {
        auto &block = m_functionCodeBlocks.emplace_back(std::string(node->identifier()));
        static_cast<void>(block);

        for (const auto &child : node->children()) {
            switch (child->type()) {
                case parser::NodeType::RETURN_STATEMENT:
                    if (!processReturnStatement(block, static_cast<const parser::ReturnStatementNode *>(child.get())))
                        return false;
                    break;
                default:
                    logger::error("CodeGenerator: illegal child of function! Type: {}",
                                  parser::toString(child->type()));
                    return false;
            }
        }

        return true;
    }

    bool
    CodeGenerator::run() noexcept {
        for (const auto &child : m_rootNode->children()) {
            CHERRY_ASSERT(child->type() == parser::NodeType::FUNCTION_DEFINITION);

            const auto *node = static_cast<const parser::FunctionDefinitionNode *>(child.get());
            if (!processFunctionDefinition(node)) {
                logger::error("CodeGenerator: failed to process function definition of \"{}\"", node->identifier());
                return false;
            }
        }

        return true;
    }

} // namespace codegen
