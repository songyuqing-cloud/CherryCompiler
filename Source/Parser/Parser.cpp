/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Parser/FunctionDefinitionNode.hpp"
#include "Source/Parser/ReturnStatementNode.hpp"

namespace parser {

    std::unique_ptr<ParentNode>
    Parser::consumeCompoundStatement() noexcept {
        if (!consumePunctuator(lexer::PunctuatorType::LEFT_CURLY_BRACKET)) {
            logger::error("Parser: expected '{', got: {}", m_it->toString());
            return nullptr;
        }

        auto node = std::make_unique<ParentNode>(NodeType::COMPOUND_STATEMENT);
        auto *const currentBlockLevel = m_currentBlockLevel;
        m_currentBlockLevel = node.get();

        while (m_it != std::cend(m_tokens)) {
            if (consumePunctuator(lexer::PunctuatorType::RIGHT_CURLY_BRACKET)) {
                m_currentBlockLevel = currentBlockLevel;
                return node;
            }

            if (!processToken()) {
                logger::error("Parser: consumeCompoundStatement failed to invoke processToken()");
                m_currentBlockLevel = currentBlockLevel;
                return nullptr;
            }
        }

        logger::error("Parser: unexpected eof in consumeCompoundStatement");
        m_currentBlockLevel = currentBlockLevel;
        return nullptr;
    }

    bool
    Parser::consumeDeclaration() noexcept {
        const auto returnType = std::get<lexer::KeywordType>(*m_it);
        ++m_it;

        if (!peekIdentifier()) {
            logger::error("Parser: expected identifier, got: {}", m_it->toString());
            return false;
        }

        const auto &identifier = std::get<std::string>(*m_it);
        ++m_it;

        if (!consumePunctuator(lexer::PunctuatorType::LEFT_PARENTHESIS)) {
            logger::error("Parser: expected '(', got: {}", m_it->toString());
            return false;
        }

        static_cast<void>(returnType);
        return consumeFunctionDeclaration({}, identifier);
    }

    bool
    Parser::consumeFunctionDeclaration(Type returnType, const std::string &identifier) noexcept {
        if (!consumePunctuator(lexer::PunctuatorType::RIGHT_PARENTHESIS)) {
            logger::error("Parser: expected ')', got: {}", m_it->toString());
            return false;
        }

        if (consumePunctuator(lexer::PunctuatorType::SEMICOLON)) {
            // TODO a function declaration
            CHERRY_ASSERT(false);
            return false;
        }

        auto block = consumeCompoundStatement();
        if (!block)
            return false;

        m_rootNode.children().push_back(
                std::make_unique<FunctionDefinitionNode>(
                        std::move(returnType),
                        std::string(identifier),
                        std::move(block->children())
                )
        );
        return true;
    }

    bool
    Parser::consumeKeyword(lexer::KeywordType keyword) noexcept {
        if (peekKeyword(keyword)) {
            ++m_it;
            return true;
        }

        return false;
    }

    bool
    Parser::consumePunctuator(lexer::PunctuatorType punctuatorType) noexcept {
        if (peekPunctuator(punctuatorType)) {
            ++m_it;
            return true;
        }

        return false;
    }

    bool
    Parser::consumeReturnStatement() noexcept {
        if (!consumeKeyword(lexer::KeywordType::RETURN))
            return false;

        if (m_currentBlockLevel == nullptr) {
            logger::error("Parser: found return statement outside of a function definition");
            return false;
        }

        if (consumePunctuator(lexer::PunctuatorType::SEMICOLON)) {
            m_currentBlockLevel->children().push_back(std::make_unique<ReturnStatementNode>(nullptr));
            return true;
        }

        auto expression = consumeExpression();
        if (expression == nullptr) {
            logger::trace("Parser: failed to produce expression for return statement");
            return false;
        }

        if (!consumePunctuator(lexer::PunctuatorType::SEMICOLON)) {
            logger::error("Parser: expected ';' after return statement, got: {}", m_it->toString());
            return true;
        }

        m_currentBlockLevel->children().push_back(std::make_unique<ReturnStatementNode>(std::move(expression)));
        return true;
    }

    bool
    Parser::peekIdentifier() const noexcept {
        if (m_it == std::cend(m_tokens))
            return false;
        return m_it->type() == lexer::TokenType::IDENTIFIER;
    }

    bool
    Parser::peekKeyword(lexer::KeywordType keyword) const noexcept {
        if (m_it == std::cend(m_tokens))
            return false;
        if (m_it->type() != lexer::TokenType::KEYWORD)
            return false;
        return std::get<lexer::KeywordType>(*m_it) == keyword;
    }

    bool
    Parser::peekPunctuator(lexer::PunctuatorType punctuatorType) const noexcept {
        if (m_it == std::cend(m_tokens))
            return false;
        if (m_it->type() != lexer::TokenType::PUNCTUATOR)
            return false;
        return std::get<lexer::PunctuatorType>(*m_it) == punctuatorType;
    }

    bool
    Parser::processToken() noexcept {
        if (peekKeyword(lexer::KeywordType::INT)) {
            return consumeDeclaration();
        }

        if (peekKeyword(lexer::KeywordType::RETURN)) {
            return consumeReturnStatement();
        }

        return false;
    }

    bool
    Parser::run() noexcept {
        m_it = std::cbegin(m_tokens);

        while (m_it != std::cend(m_tokens)) {
            if (!processToken())
                return false;
        }

        return true;
    }

} // namespace parser
