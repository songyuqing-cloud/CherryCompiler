/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Parser/Expressions/Expression.hpp"
#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace parser {

    void
    Parser::printExpression(const Expression *expression, std::size_t depth) noexcept {
        const std::string prefix(depth * 4, ' ');

        logger::debug("{}Expression{{type={}}}", prefix, toString(expression->type()));
        switch (expression->type()) {
            case ExpressionType::PRIMARY_DOUBLE:
                logger::debug("{}Data: {}", prefix, static_cast<const PrimaryExpression<double> *>(expression)->data());
                break;
            case ExpressionType::PRIMARY_STRING_LITERAL:
                logger::debug("{}Data: \"{}\"", prefix, static_cast<const PrimaryExpression<std::string> *>(expression)->data());
                break;
            case ExpressionType::PRIMARY_UNSIGNED:
                logger::debug("{}Data: {}", prefix, static_cast<const PrimaryExpression<std::uint64_t> *>(expression)->data());
                break;
            default:
                break;
        }
    }

} // namespace parser
