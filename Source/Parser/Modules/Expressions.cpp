/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Parser/Expressions/PrimaryExpression.hpp"

namespace parser {

    using Iterator = std::vector<lexer::Token>::const_iterator;

    std::unique_ptr<Expression>
    consumePrimaryExpression(Iterator &iterator) noexcept {
        if (iterator->type() == lexer::TokenType::NUMBER_UNSIGNED) {
            return std::make_unique<PrimaryExpression<std::uint64_t>>(std::get<std::uint64_t>(*iterator++));
        }

        if (iterator->type() == lexer::TokenType::STRING) {
            return std::make_unique<PrimaryExpression<std::string>>(std::get<std::string>(*iterator++));
        }

        return nullptr;
    }

    std::unique_ptr<Expression>
    Parser::consumeExpression() noexcept {
        return consumePrimaryExpression(m_it);
    }

} // namespace parser
