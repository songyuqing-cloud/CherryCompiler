/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Parser.hpp"

#include "Source/Parser/FunctionDefinitionNode.hpp"
#include "Source/Parser/ReturnStatementNode.hpp"

namespace parser {

    static void
    printParentNode(const std::string &prefix, std::size_t depth, const Node *nodePtr) noexcept {
        const auto *node = static_cast<const ParentNode *>(nodePtr);
        logger::debug("{}Child count: {}", prefix, std::size(node->children()));
        for (const auto &child : node->children()) {
            Parser::printAST(child.get(), depth + 1);
        }
    }

    static void
    printFunctionDefinition(const std::string &prefix, std::size_t depth, const Node *nodePtr) noexcept {
        const auto *node = static_cast<const FunctionDefinitionNode *>(nodePtr);
        logger::debug("{}Identifier: \"{}\"", prefix, node->identifier());
        printParentNode(prefix, depth, node);
    }

    void
    Parser::printAST(const Node *node, std::size_t depth) noexcept {
        const std::string prefix(depth * 4, ' ');

        logger::debug("{}Node{{type={}}}", prefix, toString(node->type()));
        switch (node->type()) {
            case NodeType::ROOT:
            case NodeType::COMPOUND_STATEMENT:
                printParentNode(prefix, depth, node);
                break;
            case NodeType::FUNCTION_DEFINITION:
                printFunctionDefinition(prefix, depth, node);
                break;
            case NodeType::RETURN_STATEMENT: {
                const auto *statement = static_cast<const ReturnStatementNode *>(node);
                if (statement->expression() == nullptr)
                    logger::debug("{}    Expression: void");
                else
                    printExpression(statement->expression(), depth + 1);
            } break;
            default:
                break;
        }
    }

} // namespace parser
