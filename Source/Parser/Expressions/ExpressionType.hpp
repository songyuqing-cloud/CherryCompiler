/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace parser {

    enum class ExpressionType {
        EMPTY,
        PRIMARY_DOUBLE,
        PRIMARY_STRING_LITERAL,
        PRIMARY_UNSIGNED,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(ExpressionType expressionType) noexcept {
        switch (expressionType) {
            case ExpressionType::EMPTY:                     return "empty";
            case ExpressionType::PRIMARY_DOUBLE:            return "primary-double";
            case ExpressionType::PRIMARY_STRING_LITERAL:    return "primary-string-literal";
            case ExpressionType::PRIMARY_UNSIGNED:          return "primary-unsigned";
            default:                                        return "(invalid)";
        }
    }

} // namespace parser
