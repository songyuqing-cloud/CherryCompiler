/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Lexer/Token.hpp"

#include <fmt/format.h>

#include "Source/Base/Assert.hpp"

namespace lexer {

    std::string
    Token::toString() const noexcept {
        switch (m_type) {
            case TokenType::IDENTIFIER:
                return fmt::format("identifier({})", std::get<std::string>(*this));
            case TokenType::KEYWORD:
                return fmt::format("keyword({})", std::get<KeywordType>(*this));
            case TokenType::NUMBER_UNSIGNED:
                return fmt::format("number-unsigned({})", std::get<std::uint64_t>(*this));
            case TokenType::PUNCTUATOR:
                return fmt::format("punctuator({})", lexer::toString(std::get<PunctuatorType>(*this)));
            case TokenType::STRING:
                return fmt::format("string-literal({})", std::get<std::string>(*this));
            case TokenType::NUMBER_DECIMAL:
            default:
                CHERRY_ASSERT(false)
                return "";
        }
    }

} // namespace lexer
