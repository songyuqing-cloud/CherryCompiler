/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Lexer/Lexer.hpp"

#include <iterator> // for std::next

#include <cstdlib> // for std::strtoll

#include "Include/text/Tools.hpp"
#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"

namespace lexer {

    bool
    Lexer::run() noexcept {
        if (!m_fileInput.isOK()) {
            logger::error("Lexer: failed to load file!");
            return false;
        }

        m_characters.reserve(std::size(m_fileInput));

        char32_t character;
        while (m_fileInput.readChar(&character)) {
            m_characters.push_back(character);
        }

        bool inBlockComment{false};
        bool inLineComment{false};

        for (m_it = std::cbegin(m_characters); m_it != std::cend(m_characters);) {
            const auto next = std::next(m_it);
            character = *m_it;

            if (inLineComment) {
                if (consumeLineEnd()) {
                    inLineComment = false;
                } else {
                    ++m_it;
                }
                continue;
            }

            if (inBlockComment) {
                if (character == text::ASTERISK && next != std::cend(m_characters) && *next == text::SOLIDUS) {
                    m_it += 2;
                    inBlockComment = false;
                    continue;
                }
                if (next != std::cend(m_characters))
                    logger::debug("character={} next={}", static_cast<char>(character), static_cast<char>(*next));

                ++m_it;
                continue;
            }

            if (consumeWhitespace()) {
                continue;
            }

            if (character == text::SOLIDUS) {
                if (*next == text::SOLIDUS) {
                    m_it += 2;
                    inLineComment = true;
                    continue;
                }
                if (*next == text::ASTERISK) {
                    m_it += 2;
                    inBlockComment = true;
                    continue;
                }
            }

            if (text::isASCIIAlpha(character)) {
                if (!consumeIdentifier(character))
                    return false;
            } else if (text::isDigit(character)) {
                if (!consumeNumber(character))
                    return false;
            } else if (!consumePunctuator(character)) {
                return false;
            }
        }

        for (const auto &token : m_tokens) {
            logger::debug("Token type={}", toString(token.type()));
        }
        return true;
    }

    void
    Lexer::appendCodePointToString(std::string &string, char32_t character) noexcept {
        CHERRY_ASSERT(character <= 0x7F);
        string += static_cast<char>(character);
    }

    bool
    Lexer::consumeIdentifier(char32_t character) noexcept {
        std::string buffer{};
        appendCodePointToString(buffer, character);

        ++m_it;
        while (m_it != std::cend(m_characters)) {
            if (!text::isASCIIAlpha(*m_it)
                    && !text::isDigit(*m_it)
                    && *m_it != text::LOW_LINE) {
                const auto keyword = convertStringToKeyword(buffer);

                if (keyword != KeywordType::INVALID) {
                    m_tokens.emplace_back(TokenType::KEYWORD, keyword);
                    return true;
                }

                m_tokens.emplace_back(TokenType::IDENTIFIER, std::move(buffer));
                return true;
            }

            appendCodePointToString(buffer, *m_it);
            m_it += 1;
        }

        return false;
    }

    bool
    Lexer::consumeLineEnd() noexcept {
        if (*m_it == text::LINE_FEED) {
            ++m_it;
            return true;
        }

        if (*m_it == text::CARRIAGE_RETURN) {
            ++m_it;

            if (*m_it == text::LINE_FEED) {
                ++m_it;
            }

            return true;
        }

        return false;
    }

    bool
    Lexer::consumeNumber(char32_t character) noexcept {
        std::string buffer{};
        appendCodePointToString(buffer, character);

        ++m_it;
        while (m_it != std::cend(m_characters)) {
            if (!text::isDigit(*m_it)) {
                char *end{nullptr};
                errno = 0;
                const auto integer = std::strtoll(buffer.c_str(), &end, 10);
                if (end == nullptr || *end != text::NULL_CHARACTER || errno != 0) {
                    return false;
                }

                m_tokens.emplace_back(TokenType::NUMBER_UNSIGNED, static_cast<std::uint64_t>(integer));
                return true;
            }
            
            appendCodePointToString(buffer, *m_it);
            ++m_it;
        }

        return false;
    }

    bool
    Lexer::consumePunctuator(char32_t character) noexcept {
        PunctuatorType type{PunctuatorType::INVALID};

        ++m_it;
        switch (character) {
            case text::LEFT_PARENTHESIS:
                type = PunctuatorType::LEFT_PARENTHESIS;
                break;
            case text::RIGHT_PARENTHESIS:
                type = PunctuatorType::RIGHT_PARENTHESIS;
                break;
            case text::PLUS_SIGN:
                type = PunctuatorType::ADDITION;
                break;
            case text::HYPHEN_MINUS:
                type = PunctuatorType::SUBTRACTION;
                break;
            case text::SEMICOLON:
                type = PunctuatorType::SEMICOLON;
                break;
            case text::LEFT_SQUARE_BRACKET:
                type = PunctuatorType::LEFT_SQUARE_BRACKET;
                break;
            case text::RIGHT_SQUARE_BRACKET:
                type = PunctuatorType::RIGHT_SQUARE_BRACKET;
                break;
            case text::LEFT_CURLY_BRACKET:
                type = PunctuatorType::LEFT_CURLY_BRACKET;
                break;
            case text::RIGHT_CURLY_BRACKET:
                type = PunctuatorType::RIGHT_CURLY_BRACKET;
                break;
            default:
                break;
        }

        if (type == PunctuatorType::INVALID) {
            std::string str{};
            appendCodePointToString(str, character);
            logger::error("Lexer: invalid character: {}", str);
            return false;
        }

        m_tokens.emplace_back(TokenType::PUNCTUATOR, type);
        return true;
    }

    bool
    Lexer::consumeWhitespace() noexcept {
        // TODO what is defined as white-space?
        bool foundWhitespace{false};
        while (m_it != std::cend(m_characters)) {
            switch (*m_it) {
                case text::CARRIAGE_RETURN:
                case text::LINE_TABULATION:
                case text::LINE_FEED:
                case text::SPACE:
                    foundWhitespace = true;
                    ++m_it;
                    break;
                default:
                    return foundWhitespace;
            }
        }

        return foundWhitespace;
    }

} // namespace lexer
