/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <vector>

#include <cstddef>

#include "Source/Base/FileInput.hpp"
#include "Source/Lexer/Token.hpp"

namespace lexer {

    class Lexer {
        std::vector<char32_t> m_characters{};
        base::FileInput m_fileInput;

        std::vector<char32_t>::const_iterator m_it{};
        std::vector<Token> m_tokens{};

        static void
        appendCodePointToString(std::string &string, char32_t character) noexcept;

        [[nodiscard]] bool
        consumeIdentifier(char32_t) noexcept;

        [[nodiscard]] bool
        consumeLineEnd() noexcept;

        [[nodiscard]] bool
        consumeNumber(char32_t) noexcept;

        [[nodiscard]] bool
        consumePunctuator(char32_t) noexcept;

        [[nodiscard]] bool
        consumeWhitespace() noexcept;

    public:
        [[nodiscard]] inline explicit
        Lexer(const std::string &fileName)
                : m_fileInput(fileName) {
        }

        [[nodiscard]] bool
        run() noexcept;

        [[nodiscard]] inline std::vector<Token> &
        tokens() noexcept {
            return m_tokens;
        }

        [[nodiscard]] inline const std::vector<Token> &
        tokens() const noexcept {
            return m_tokens;
        }
    };

} // namespace lexer
