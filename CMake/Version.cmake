# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 0)

add_compile_definitions(CHERRY_VERSION_MAJOR=${VERSION_MAJOR}
						CHERRY_VERSION_MINOR=${VERSION_MINOR}
						CHERRY_VERSION_PATCH=${VERSION_PATCH})
